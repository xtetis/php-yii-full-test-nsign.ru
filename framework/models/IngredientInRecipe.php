<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingredient_in_recipe".
 *
 * @property int $id Первичный ключ
 * @property int $id_ingredient Ссылка на ингредиент
 * @property int $id_recipe Ссылка на рецепт
 *
 * @property Ingredient $ingredient
 * @property Recipe $recipe
 */
class IngredientInRecipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient_in_recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ingredient', 'id_recipe'], 'required'],
            [['id_ingredient', 'id_recipe'], 'integer'],
            [['id_ingredient'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['id_ingredient' => 'id']],
            [['id_recipe'], 'exist', 'skipOnError' => true, 'targetClass' => Recipe::className(), 'targetAttribute' => ['id_recipe' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'id_ingredient' => 'Ссылка на ингредиент',
            'id_recipe' => 'Ссылка на рецепт',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'id_ingredient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Recipe::className(), ['id' => 'id_recipe']);
    }
}
