<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Ingredient;

/* @var $this yii\web\View */
/* @var $model app\models\Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    

    <?= Html::label('Ингредиенты', 'ingredients') ?>
    <?= Html::dropDownList(
            'ingredients[]', 
            ArrayHelper::map($model->getIngredientInRecipes()->all(), 'id', 'id_ingredient'), 
            ArrayHelper::map(Ingredient::find()->all(), 'id', 'name'), 
            ['class' => 'form-control', 'multiple' => true],
    ); ?>

    



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
