<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingredient".
 *
 * @property int $id Первичный ключ
 * @property string $name Имя
 * @property string $description Описание ингредиента
 * @property int $active Активность
 *
 * @property IngredientInRecipe[] $ingredientInRecipes
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'name' => 'Имя',
            'description' => 'Описание ингредиента',
            'active' => 'Активность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientInRecipes()
    {
        return $this->hasMany(IngredientInRecipe::className(), ['id_ingredient' => 'id']);
    }
}
