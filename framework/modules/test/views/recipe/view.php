<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recipe */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Recipes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="recipe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
                'label' => 'Ингредиенты',

                'value' => function ($model) {
                    
                    $ret_ingredient_array = [];
                    $model__IngredientInRecipe_all = $model->getIngredientInRecipes()->all();
                    if ($model__IngredientInRecipe_all)
                    {
                        foreach ($model__IngredientInRecipe_all as $model__IngredientInRecipe_item) 
                        {
                            $model_Ingredient = $model__IngredientInRecipe_item->getIngredient()->one();
                            if ($model_Ingredient)
                            {
                                $ret_ingredient_array[]=$model_Ingredient->name;
                            }
                        }
                    }
                    
                    return implode(', ',$ret_ingredient_array);
                }
            ],
        ],
    ]) ?>

</div>
