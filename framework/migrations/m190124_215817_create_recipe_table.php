<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipe`.
 */
class m190124_215817_create_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('recipe', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200),
            'description' => $this->string(200),
        ]);

        $recipe = array(
            array('id' => '1','name' => 'Рагу','description' => 'смесь овощей с мясом'),
            array('id' => '2','name' => 'Пюре','description' => 'толченая картошка'),
            array('id' => '3','name' => 'Шашлык','description' => 'На костре'),
            array('id' => '4','name' => 'Морковь по-корейски','description' => 'ывфывфыв'),
            array('id' => '5','name' => 'Хлеб','description' => 'фывфыв'),
            array('id' => '6','name' => 'Вареники с капустой','description' => '11111'),
            array('id' => '7','name' => 'Вареники с картошкой','description' => 'фывфыв'),
            array('id' => '8','name' => 'Чебуреки','description' => 'фвфыв')
          );

        foreach ($recipe as $v) {
            $this->insert('recipe', $v);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('recipe');
        $this->dropTable('recipe');
    }
}
