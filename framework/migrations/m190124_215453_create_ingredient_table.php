<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ingredient`.
 */
class m190124_215453_create_ingredient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ingredient', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200),
            'description' => $this->string(200),
            'active' => $this->integer()->defaultValue(1),
        ]);

        $ingredient = array(
            array('id' => '1','name' => 'Картошка','description' => 'asdasd1','active' => '1'),
            array('id' => '3','name' => 'Мясо','description' => 'фывфывфыв','active' => '1'),
            array('id' => '4','name' => 'Морковь','description' => 'Обычная морковь','active' => '1'),
            array('id' => '5','name' => 'Соль','description' => 'Поваренная соль','active' => '1'),
            array('id' => '7','name' => 'Капуста','description' => 'Капуста белокачанная','active' => '1'),
            array('id' => '8','name' => 'Мука','description' => 'Пшеничная мука','active' => '1'),
            array('id' => '9','name' => 'Вода','description' => 'Из-под крана','active' => '1'),
            array('id' => '10','name' => 'Масло','description' => 'Подсолнечное','active' => '1'),
            array('id' => '11','name' => 'Сахар','description' => 'Белый','active' => '1')
          );

        foreach ($ingredient as $v) {
            $this->insert('ingredient', $v);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('ingredient');
        $this->dropTable('ingredient');
    }
}
