<?php

namespace app\models;

use Yii;



/**
 * This is the model class for table "recipe".
 *
 * @property int $id Первичный ключ
 * @property string $name Имя
 * @property string $description Описание рецепта
 *
 * @property IngredientInRecipe[] $ingredientInRecipes
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Первичный ключ',
            'name' => 'Имя',
            'description' => 'Описание рецепта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientInRecipes()
    {
        return $this->hasMany(IngredientInRecipe::className(), ['id_recipe' => 'id']);
    }



    /**
     * Связывает
     */
    public function setIngredientsToRecipe($id,$ingredient_id_array)
    {
        foreach ($ingredient_id_array as $ingredients_id) 
        {
            $model_IngredientInRecipe = IngredientInRecipe::find()->where(
                                                ["id_recipe" =>$id,"id_ingredient"=>$ingredients_id]
                                            )->one();
            if (!$model_IngredientInRecipe)
            {
                $model_IngredientInRecipe = new IngredientInRecipe();
                $model_IngredientInRecipe->id_ingredient = $ingredients_id;
                $model_IngredientInRecipe->id_recipe = $id;
                $model_IngredientInRecipe->save();
            }
        }

        IngredientInRecipe::deleteAll(
                [
                    'AND', 
                    'id_recipe = :id_recipe', 
                    ['NOT IN', 'id_ingredient', $ingredient_id_array]
                ], 
                [':id_recipe' => $id]
            );    
    }



}
