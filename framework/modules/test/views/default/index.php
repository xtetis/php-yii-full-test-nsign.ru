<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Ingredient;
use yii\grid\GridView;

?>
<div class="test-default-index">
    <h1>Поиск рецептов</h1>
    <div class="row">
        <div class="col-md-3">
            

            <?php $form = ActiveForm::begin(); ?>


                <?= Html::label('Ингредиенты', 'ingredients') ?>

                <?
                $ingredient_list = ArrayHelper::map(Ingredient::find()->where(["active" =>1])->all(), 'id', 'name');
                $ingredient_selected = $ingredient_id_array;
                $options = ['separator' => '<br>']
                ?>

                <?= Html::checkboxList('ingredients[]',$ingredient_selected,$ingredient_list,$options); ?>

                <div class="form-group">
                    <?= Html::submitButton('Поиск', ['class' => 'btn btn-success']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-9">
            <? if($count_selected_ingredients>5): ?>
                <div class="alert alert-info" role="alert">Выбирать можно не более 5 элементов</div>
            <? elseif($count_selected_ingredients<2): ?>
                <div class="alert alert-info" role="alert">Выберите больше ингредиентов</div>
            <? elseif($dataProvider->getTotalCount()<1): ?>
                <div class="alert alert-info" role="alert">Ничего не найдено</div>
            <? else: ?>
            
                <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                //'filterOptions'=>
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'description:ntext',
                    [
                        'label' => 'Ингредиенты',

                        'value' => function ($model) {
                            
                            $ret_ingredient_array = [];
                            $model__IngredientInRecipe_all = $model->getIngredientInRecipes()->all();
                            if ($model__IngredientInRecipe_all)
                            {
                                foreach ($model__IngredientInRecipe_all as $model__IngredientInRecipe_item) 
                                {
                                    $model_Ingredient = $model__IngredientInRecipe_item->getIngredient()->one();
                                    if ($model_Ingredient)
                                    {
                                        $ret_ingredient_array[]=$model_Ingredient->name;
                                    }
                                }
                            }
                            
                            return implode(', ',$ret_ingredient_array);
                        }
                    ],
                ],
                ]); 
                
                
                
                ?>
            <? endif; ?>
        </div>
    </div>

</div>
