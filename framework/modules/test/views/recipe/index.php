<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RecipeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рецепты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recipe-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Recipe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',
            [
                'label' => 'Ингредиенты',

                'value' => function ($model) {
                    
                    $ret_ingredient_array = [];
                    $model__IngredientInRecipe_all = $model->getIngredientInRecipes()->all();
                    if ($model__IngredientInRecipe_all)
                    {
                        foreach ($model__IngredientInRecipe_all as $model__IngredientInRecipe_item) 
                        {
                            $model_Ingredient = $model__IngredientInRecipe_item->getIngredient()->one();
                            if ($model_Ingredient)
                            {
                                $ret_ingredient_array[]=$model_Ingredient->name;
                            }
                        }
                    }
                    
                    return implode(', ',$ret_ingredient_array);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
