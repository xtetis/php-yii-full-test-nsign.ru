<div class="test-default-index">
    <div>
        <div class="btn-group" role="group" aria-label="...">
            <a href="/<?=$this->context->module->id ?>/<?=$this->context->module->module->controller->id ?>/ingredient" class="btn btn-default">Ингредиенты</a>
            <a href="/<?=$this->context->module->id ?>/<?=$this->context->module->module->controller->id ?>/recipe" class="btn btn-default">Рецепты</a>
        </div>
    </div>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>