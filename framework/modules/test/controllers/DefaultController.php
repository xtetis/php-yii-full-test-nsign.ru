<?php

namespace app\modules\test\controllers;

use Yii;
use yii\web\Controller;
use app\models\Recipe;
use app\models\RecipeSearch;

/**
 * Default controller for the `test` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $ingredient_id_array = Yii::$app->request->post('ingredients',[]); 
        
        
        if  (
                (count($ingredient_id_array)>1) &&
                (count($ingredient_id_array)<6)
            )
        {
            $searchModel = new RecipeSearch();
            //var_dump(Yii::$app->request->queryParams); exit;
            $dataProvider = $searchModel->searchFront($ingredient_id_array);

            $searchModel->searchAllInclusive($ingredient_id_array);
        }
        
        //return $this->render('index');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ingredient_id_array' =>$ingredient_id_array,
            'count_selected_ingredients' =>count($ingredient_id_array)
        ]);
    }

       
    
    public function actionAbout()
    {
        echo 123;
    }
}
