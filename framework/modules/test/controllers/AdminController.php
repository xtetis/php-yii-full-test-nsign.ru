<?php

namespace app\modules\test\controllers;

use yii\web\Controller;
use app\models\Ingredient;
use app\models\IngredientSearch;

/**
 * Default controller for the `test` module
 */
class AdminController extends Controller
{


    public function init()
    {
        parent::init();

        if (\Yii::$app->session->get('auth_requests',0) != 1)
        {
            if  (
                    (!isset($_SERVER['PHP_AUTH_USER']))     ||
                    (!isset($_SERVER['PHP_AUTH_PW']))       ||
                    ($_SERVER['PHP_AUTH_USER'] != 'admin')  ||
                    (md5(md5($_SERVER['PHP_AUTH_PW'])) != 'b99937535a8405c947a37947cb775575') //qweqweqwe
                )
            {
                

                unset($_SERVER['PHP_AUTH_PW']);
                unset($_SERVER['PHP_AUTH_PW']);
                header('WWW-Authenticate: Basic realm=""');
                header('HTTP/1.0 401 Unauthorized');
                exit;
            }
            else
            {
                \Yii::$app->session->set('auth_requests',1);
            }
        }
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

       
    
    public function actionIngredient()
    {
        $searchModel = new IngredientSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAbout()
    {
        echo 456;
    }
}
