<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ingredient_in_recipe`.
 * Has foreign keys to the tables:
 *
 * - `ingredient`
 * - `recipe`
 */
class m190124_220048_create_ingredient_in_recipe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ingredient_in_recipe', [
            'id' => $this->primaryKey(),
            'id_ingredient' => $this->integer()->notNull(),
            'id_recipe' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_ingredient`
        $this->createIndex(
            'idx-ingredient_in_recipe-id_ingredient',
            'ingredient_in_recipe',
            'id_ingredient'
        );

        // add foreign key for table `ingredient`
        $this->addForeignKey(
            'fk-ingredient_in_recipe-id_ingredient',
            'ingredient_in_recipe',
            'id_ingredient',
            'ingredient',
            'id',
            'CASCADE'
        );

        // creates index for column `id_recipe`
        $this->createIndex(
            'idx-ingredient_in_recipe-id_recipe',
            'ingredient_in_recipe',
            'id_recipe'
        );

        // add foreign key for table `recipe`
        $this->addForeignKey(
            'fk-ingredient_in_recipe-id_recipe',
            'ingredient_in_recipe',
            'id_recipe',
            'recipe',
            'id',
            'CASCADE'
        );


        $ingredient_in_recipe = array(
            array('id' => '2','id_ingredient' => '3','id_recipe' => '1'),
            array('id' => '3','id_ingredient' => '1','id_recipe' => '2'),
            array('id' => '4','id_ingredient' => '1','id_recipe' => '1'),
            array('id' => '5','id_ingredient' => '4','id_recipe' => '1'),
            array('id' => '6','id_ingredient' => '5','id_recipe' => '1'),
            array('id' => '7','id_ingredient' => '5','id_recipe' => '2'),
            array('id' => '11','id_ingredient' => '3','id_recipe' => '3'),
            array('id' => '12','id_ingredient' => '5','id_recipe' => '3'),
            array('id' => '15','id_ingredient' => '5','id_recipe' => '5'),
            array('id' => '16','id_ingredient' => '8','id_recipe' => '5'),
            array('id' => '17','id_ingredient' => '5','id_recipe' => '6'),
            array('id' => '18','id_ingredient' => '7','id_recipe' => '6'),
            array('id' => '19','id_ingredient' => '8','id_recipe' => '6'),
            array('id' => '20','id_ingredient' => '4','id_recipe' => '4'),
            array('id' => '21','id_ingredient' => '5','id_recipe' => '4'),
            array('id' => '22','id_ingredient' => '1','id_recipe' => '7'),
            array('id' => '23','id_ingredient' => '5','id_recipe' => '7'),
            array('id' => '24','id_ingredient' => '8','id_recipe' => '7'),
            array('id' => '26','id_ingredient' => '3','id_recipe' => '8'),
            array('id' => '28','id_ingredient' => '5','id_recipe' => '8'),
            array('id' => '31','id_ingredient' => '8','id_recipe' => '8'),
            array('id' => '32','id_ingredient' => '9','id_recipe' => '6'),
            array('id' => '33','id_ingredient' => '9','id_recipe' => '7')
          );

        foreach ($ingredient_in_recipe as $v) {
            $this->insert('ingredient_in_recipe', $v);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `ingredient`
        $this->dropForeignKey(
            'fk-ingredient_in_recipe-id_ingredient',
            'ingredient_in_recipe'
        );

        // drops index for column `id_ingredient`
        $this->dropIndex(
            'idx-ingredient_in_recipe-id_ingredient',
            'ingredient_in_recipe'
        );

        // drops foreign key for table `recipe`
        $this->dropForeignKey(
            'fk-ingredient_in_recipe-id_recipe',
            'ingredient_in_recipe'
        );

        // drops index for column `id_recipe`
        $this->dropIndex(
            'idx-ingredient_in_recipe-id_recipe',
            'ingredient_in_recipe'
        );


        $this->delete('ingredient_in_recipe');

        $this->dropTable('ingredient_in_recipe');
    }
}
