<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recipe;

/**
 * RecipeSearch represents the model behind the search form of `app\models\Recipe`.
 */
class RecipeSearch extends Recipe
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recipe::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load([$params]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }



    public function searchFront($ingredient_id_array)
    {
        $query = $this->searchAllInclusive($ingredient_id_array);

        if (!$query->count())
        {
            $query = $this->searchNotAllInclusive($ingredient_id_array);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load([]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    





    /**
     * Выводятся только те рецепты у которых только те ингредиенты, что выбраны 
     * (в том числе и если рецепт состоит из меньшего количества ингредииентов)
     */
    public function searchAllInclusive($ingredient_id_array)
    {

        $ingredient_str = implode(',', $ingredient_id_array);
        $sql_in = "
        SELECT DISTINCT
            ir.id_recipe
        FROM
            ingredient_in_recipe ir
        WHERE
            ir.id_ingredient IN (".$ingredient_str.")
            AND
            ir.id_ingredient IN
            (
                SELECT 
                    i.id
                FROM	
                    ingredient i
                WHERE
                    i.active = 1   
            )
        
            AND
            
            ir.id_recipe NOT IN
            (
                SELECT
                    t.id_recipe
                FROM
                    ingredient_in_recipe t
                WHERE
                    t.id_ingredient NOT IN (".$ingredient_str.")
            )
        ";
        return Recipe::find()->where('id IN ('.$sql_in.')');

    }














    /**
     * Если найдены блюда с частичным совпадением ингредиентов ­ вывести
     * в порядке уменьшения совпадения ингредиентов вплоть до 2­х.
     */
    public function searchNotAllInclusive($ingredient_id_array)
    {

        $ingredient_str = implode(',', $ingredient_id_array);
        $sql_in = "
        SELECT 
            id_recipe
        FROM
            (
                SELECT  
                    ir.id_recipe,
                    count(ir.id_ingredient) c
                FROM 
                    ingredient_in_recipe ir
                WHERE 
                    ir.id_ingredient IN (".$ingredient_str.")
                    AND
                    ir.id_ingredient IN
                    (
                        SELECT 
                            i.id
                        FROM	
                            ingredient i
                        WHERE
                            i.active = 1   
                    )
                GROUP BY 
                    ir.id_recipe 
                HAVING 
                    count(ir.id_ingredient) >1
                ORDER BY c DESC
            ) tmp
    
        ";
        return Recipe::find()->where('id IN ('.$sql_in.')');

    }



}
